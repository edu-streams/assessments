import React from "react";

export const useFormattedDate = (date) => {
  const formatDate = (inputDate) => {
    const newDate = new Date(inputDate);

    const day = newDate.getDate();
    const month = newDate.getMonth() + 1;
    const year = newDate.getFullYear();

    const outputDate =
      (day < 10 ? "0" : "") +
      day +
      "." +
      (month < 10 ? "0" : "") +
      month +
      "." +
      year;

    return outputDate;
  };

  const formattedDate = React.useMemo(() => formatDate(date), [date]);

  return formattedDate;
};
