import axios from "axios";

const fetchBaseUrl = async () => {
  const { data } = await axios.get(
    `${process.env.PUBLIC_URL}/config/config.json`
  );
  axios.defaults.baseURL = data.apiEndpoint;
};

export const fetchUsers = async (streamId) => {
  try {
    if (!axios.defaults.baseURL) {
      await fetchBaseUrl();
    }

    const response = await axios.get(
      // FIXME: student&grading -> student_grading
      `/student_grading/${streamId}`
    );
    return response.data;
  } catch (e) {
    return console.log(e);
  }
};

export const addUser = async (value) => {
  try {
    if (!axios.defaults.baseURL) {
      await fetchBaseUrl();
    }

    const response = await axios.post("/stream_student", value);
    return response.data;
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const getUserGradingsById = async (id) => {
  try {
    if (!axios.defaults.baseURL) {
      await fetchBaseUrl();
    }

    const response = await axios.get(`/gradings/${id}`);
    return response.data;
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const getUsernameById = async (id) => {
  try {
    if (!axios.defaults.baseURL) {
      await fetchBaseUrl();
    }

    const response = await axios.get(`/student/${id}`);
    return response.data.username;
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const updateGrade = async ({ data, gradingId }) => {
  try {
    const response = await axios.put(`/grading/${gradingId}`, data);
    return response.data;
  } catch (e) {
    return console.log(e);
  }
};

export const addGrade = async (data) => {
  try {
    const response = await axios.post(`/grading`, data);
    return response.data;
  } catch (e) {
    return console.log(e);
  }
};

export const endStream = async (streamId) => {
  try {
    const response = await axios.post(`/end_stream`, {
      id: streamId,
    });
    return response.data;
  } catch (e) {
    return console.log(e);
  }
};

export const startStream = async (name) => {
  try {
    if (!axios.defaults.baseURL) {
      await fetchBaseUrl();
    }

    const response = await axios.post(`/start_stream`, {
      name,
    });
    return response.data;
  } catch (e) {
    return console.log(e);
  }
};
