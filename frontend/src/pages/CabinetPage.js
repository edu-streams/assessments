import { Box, Button } from "@mui/material";
import { StudentCabinet } from "../components/StudentCabinet/StudentCabinet";
import { useEffect, useState } from "react";
import { TeacherCabinet } from "../components/TeacherCabinet/TeacherCabinet";
import * as jose from "jose";

const ErrorPage = () => {
  const [user, setUser] = useState(null);
  const [teacher, setTeacher] = useState(false);

  useEffect(() => {
    const myId = localStorage.getItem("myId");
    if (myId) {
      const userToken = localStorage.getItem(
        "logto:asnhh0px905tmbmdwlqm1:idToken"
      );

      if (userToken) {
        const claims = jose.decodeJwt(userToken);
        console.log(claims);
        const studentId = {
          name: claims.name,
          id: myId,
          img: claims.picture,
        };
        setUser(studentId);
      }
    }
  }, []);

  useEffect(() => {}, [teacher]);

  return (
    <Box>
      {!teacher ? (
        <>
          <Button
            onClick={() => setTeacher("8910d094-abf3-48c3-80f7-ef028cfb330e")}
          >
            go to Teacher Cabinet
          </Button>

          {user && <StudentCabinet studentId={user} />}

          {/* {!user && (
            <StudentCabinet
              studentId={{
                name: "Default: name 5",
                id: "c1a04b66-b7bb-48c2-8a8c-a0cba7374b8f",
              }}
            />
          )} */}
        </>
      ) : (
        <>
          <Button onClick={() => setTeacher(false)}>
            go to Student Cabinet
          </Button>

          <TeacherCabinet />
        </>
      )}
    </Box>
  );
};
export default ErrorPage;
