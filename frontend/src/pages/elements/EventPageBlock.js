import { Box, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { socket } from "../../services/socket";
import { addUser, fetchUsers } from "../../services/fetchUserData";
import * as jose from "jose";

const getStudentId = () => {
  const data = localStorage.getItem("myId");
  return data ? data : "";
};

const EventPageBlock = () => {
  const [grade, setGrade] = useState("");
  const [name, setName] = useState("");
  const [myId, setMyId] = useState(() => getStudentId());

  const [streamId, setStreamId] = useState(null);

  useEffect(() => {
    const userToken = localStorage.getItem(
      "logto:asnhh0px905tmbmdwlqm1:idToken"
    );
    if (userToken) {
      const claims = jose.decodeJwt(userToken);
      setName(claims.name);
    }
  }, []);

  useEffect(() => {
    socket.on("startStream", (data) => {
      setStreamId(data);
    });

    if (name !== "" && streamId) {
      const fetchData = async () => {
        const users = await fetchUsers(streamId);
        if (users) {
          const findDuplicateUser = users.filter(
            (user) => user.studentId === myId
          );

          if (findDuplicateUser.length === 0) {
            const userData = {
              student: {
                username: name,
              },
              streamId: streamId,
            };
            const userdata = await addUser(userData);

            socket.emit("userName", { ...userdata, username: name });
            localStorage.setItem("myId", userdata.studentId);

            setMyId(userdata.studentId);
          }
        }
      };

      fetchData();
    }
  }, [streamId]);

  useEffect(() => {
    socket.on("grade", (data) => {
      console.log(myId);
      if (myId === data.id) {
        console.log(12);
        setGrade(data);
      }
    });
  }, [grade, myId]);

  return (
    <Box sx={{ p: 2 }}>
      <Box>
        {!grade.grade && !grade.comment && (
          <Typography>Немає оцінок за цей стрім</Typography>
        )}

        <Typography>{grade.grade && <>Моя оцінка: {grade.grade}</>}</Typography>
        <Typography>
          {grade.comment && <>Коментар: {grade.comment}</>}
        </Typography>
      </Box>
    </Box>
  );
};
export default EventPageBlock;
