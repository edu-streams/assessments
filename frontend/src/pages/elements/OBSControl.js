import { UserList } from "../../components/UserList/UserList";

import { Box, Button, FormControl, Select, MenuItem } from "@mui/material";
import { useEffect, useState } from "react";
import { socket } from "../../services/socket";
import { getSubjectById } from "../../helpers/getSubjectById";
import { startStream } from "../../services/fetchUserData";
import { useSearchParams } from "react-router-dom";

export const getStreamId = () => {
  const id = JSON.parse(localStorage.getItem("streamId"));
  if (id) {
    return id.id;
  }

  return null;
};

export const getEventId = () => {
  const id = JSON.parse(localStorage.getItem("streamId"));
  if (id) {
    return id.eventId;
  }

  return null;
};

const OBSControl = () => {
  const [isDisplay, setIsDisplay] = useState(true);

  const [streamId, setStreamId] = useState(() => getStreamId());

  let [searchParams, setSearchParams] = useSearchParams();
  const eventId = searchParams.get("id");

  useEffect(() => {
    const eventIdData = getEventId();

    const fetchData = async () => {
      if (eventIdData === eventId) {
        const id = streamId;
        socket.emit("startStream", id);
      } else {
        const data = await startStream(eventId);

        if (data) {
          const id = data.id;
          // const id = "e9d25eec-d481-4e72-9150-1d519638ef94";

          setStreamId(id);
          localStorage.setItem("streamId", JSON.stringify({ id, eventId }));

          socket.emit("startStream", id);
        }
      }
    };

    fetchData();
  }, [streamId]);

  useEffect(() => {
    const resultOptions = { isDisplay };
    socket.emit("control", resultOptions);
  }, [isDisplay]);

  return (
    <Box textAlign={"center"} mt={2}>
      {streamId && <UserList streamId={streamId} />}
    </Box>
  );
};
export default OBSControl;
