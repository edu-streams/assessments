import * as React from "react";
import { Grid, styled } from "@mui/material";
import Link from "@mui/material/Link";

const Item = styled("div")(({ theme }) => ({
  textAlign: "center",
  border: "solid 1px black",
  padding: "3px",
}));

export default function DebugPage() {
  // React.useEffect(() => {
  //   localStorage.setItem(
  //     "logto:asnhh0px905tmbmdwlqm1:idToken",
  //     "eyJhbGciOiJFUzM4NCIsInR5cCI6IkpXVCIsImtpZCI6IkRaZWJQMmhnZk5KZkNFTmFmZ0NZREVCdVl1d0xSNWgybmJja29wSElWSTgifQ.eyJzdWIiOiIwODZ2NXgxcHNkZzciLCJuYW1lIjoiTGYgTGYiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EvQUFjSFR0ZDhlSExNNVFCQ1ZiR0Exa1A4ZWJFS0ZBTkhPOHZ4NXU4OWdtZzVxQT1zOTYtYyIsInVzZXJuYW1lIjpudWxsLCJhdF9oYXNoIjoibERPYkNkUGV0ZlVVSFdfQUhoeUEyZHhGcEdtdGUyZXAiLCJhdWQiOiJhc25oaDBweDkwNXRtYm1kd2xxbTEiLCJleHAiOjE2ODQ5NTExMzUsImlhdCI6MTY4NDk0NzUzNSwiaXNzIjoiaHR0cHM6Ly82cGJ2bnMubG9ndG8uYXBwL29pZGMifQ._ToeZ-zydEVeZTqsmEXOpRAgkM_8bGRVPJKaJ83COIMMfMAnecqUL6nXjQH3oMFnXjq7cvjvhL-TpvYwc860l4OXERCCMaMv8fipUEUe4j6RHOwm-p0zavB185PszVO_"
  //   );
  // }, []);

  return (
    <>
      <Grid container spacing={2}>
        {/* <Grid item xs={4}>
          <Item>
            <iframe
              width="100%"
              src="#/widget/obs-layout"
              frameBorder="0"
            ></iframe>
          </Item>
          <Link target="_blank" href="#/widget/obs-layout">
            Open OBS layout in new tab
          </Link>
        </Grid> */}
        <Grid item xs={8}>
          <Item>
            <iframe
              width="100%"
              height={500}
              src="#/widget/event-page-block"
              frameBorder="0"
            ></iframe>
          </Item>
          <Link target="_blank" href="#/widget/event-page-block">
            Open Event page block in new tab
          </Link>
        </Grid>
        <Grid item xs={4}>
          <Item>
            <iframe
              width="100%"
              height={500}
              src="#/widget/obs-control?id=123"
              frameBorder="0"
            ></iframe>
          </Item>
          <Link target="_blank" href="#/widget/obs-control">
            Open OBS control in new tab
          </Link>
        </Grid>
      </Grid>
    </>
  );
}
