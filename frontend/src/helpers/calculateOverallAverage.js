import { calculateSubjectAverages } from "./calculateSubjectAverages";

export const calculateOverallAverage = (data) => {
  console.log(1);
  const subjectAverages = calculateSubjectAverages(data);
  let total = 0;
  let count = 0;

  for (let i = 0; i < subjectAverages.length; i++) {
    total += subjectAverages[i].average;
    count++;
  }

  const overallAverage = total / count;
  return overallAverage;
};
