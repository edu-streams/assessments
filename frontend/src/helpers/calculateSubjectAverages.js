export const calculateSubjectAverages = (data) => {
  const averages = {};

  for (let i = 0; i < data.length; i++) {
    const item = data[i];
    const { subjectId, grade } = item;

    if (!averages[subjectId]) {
      averages[subjectId] = {
        subjectId: subjectId,
        total: 0,
        count: 0,
      };
    }

    averages[subjectId].total += grade;
    averages[subjectId].count++;
  }

  const result = [];

  for (const subjectId in averages) {
    const { total, count } = averages[subjectId];
    const average = total / count;
    result.push({
      subjectId: subjectId,
      average: average,
    });
  }

  return result;
};
