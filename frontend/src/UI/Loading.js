import * as React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";

export const Loading = () => {
  return (
    <Box sx={{ textAlign: "center", padding: 5 }}>
      <CircularProgress size={50} />
    </Box>
  );
};
