import { Typography } from "@mui/material";

export const GradeBox = ({ grade }) => {
  let bg;
  let borderColor;

  if (grade <= 1) {
    bg = "rgba(221, 45, 45, 0.2)";
    borderColor = "#DD2D2D";
  } else if (grade <= 2) {
    bg = "rgba(240, 115, 45, 0.2)";
    borderColor = "#F0732D";
  } else if (grade <= 3) {
    bg = "rgba(240, 169, 45, 0.2)";
    borderColor = "#F0A92D";
  } else if (grade <= 4) {
    bg = "rgba(140, 173, 44, 0.2)";
    borderColor = "#8CAD2C";
  } else if (grade <= 5) {
    bg = "rgba(60, 167, 69, 0.2)";
    borderColor = "#3CA745";
  }

  return (
    <Typography
      variant="span"
      sx={{
        border: `1px solid ${borderColor}`,
        borderRadius: "4px",
        background: `${bg}`,
        padding: "8px 24px",
      }}
    >
      {grade}
    </Typography>
  );
};
