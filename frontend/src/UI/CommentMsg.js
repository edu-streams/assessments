import MessageIcon from "@mui/icons-material/Message";
import { Box } from "@mui/material";

export const CommentMsg = ({ grade }) => {
  return (
    <Box sx={{ display: "flex", gap: "2px" }}>
      {grade ? grade : "Grade"}
      <MessageIcon color="primary" sx={{ width: 10, height: 10 }} />
    </Box>
  );
};
