import { Typography } from "@mui/material";

export const SectionTitle = ({ title }) => {
  return (
    <Typography
      pb={1}
      mb={2.5}
      sx={{ borderBottom: 1 }}
      fontSize={20}
      fontWeight={500}
    >
      {title}
    </Typography>
  );
};
