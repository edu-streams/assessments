import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { IconButton } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import Avatar from "@mui/material/Avatar";
import { useState } from "react";
import { addGrade, updateGrade } from "../../services/fetchUserData";
import { GradeButton } from "./GradeButton";
import { socket } from "../../services/socket";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "100%",
  height: "100%",
  bgcolor: "#fff",
  border: "none",
  p: 4,
};

export default function Modalk({ user, streamId }) {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [grade, setGrade] = useState(user.grade || "");
  const [comment, setComment] = useState(user.comment || "");

  const [gradingId, setGradingId] = useState(null);

  const [isDisabled, setIsDisabled] = useState(true);

  const submit = () => {
    handleClose();

    const grading = { id: user.studentId, grade, comment };
    socket.emit("grade", grading);

    const date = new Date();

    const data = {
      grade: grade === "" ? null : grade,
      comment,
      date: date.toISOString(),
      studentId: user.studentId,
      streamId,
      subjectId: "81b51e3e-7fb3-40d8-9878-62c5c116d018",
    };

    const fetchData = async () => {
      if (user.gradingId) {
        await updateGrade({ data, gradingId: user.gradingId });
      } else if (gradingId) {
        await updateGrade({ data, gradingId });
      } else {
        const gradeData = await addGrade(data);
        setGradingId(gradeData.id);
      }
    };
    fetchData();
  };

  return (
    <div>
      <GradeButton handleOpen={handleOpen} grading={{ grade, comment }} />

      <Modal
        keepMounted
        open={open}
        onClose={handleClose}
        aria-labelledby="keep-mounted-modal-title"
        aria-describedby="keep-mounted-modal-description"
      >
        <Box sx={style}>
          <Box
            sx={{
              padding: "0 16px",
              minHeight: 400,
              display: "flex",
              flexDirection: "column",
              gap: "35px",
            }}
          >
            <Box>
              <Box sx={{ textAlign: "end" }}>
                <IconButton
                  color="primary"
                  sx={{
                    borderRadius: 0,
                  }}
                  onClick={handleClose}
                >
                  <CloseIcon />
                </IconButton>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: 2,
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    gap: 2,
                  }}
                >
                  <Avatar sx={{ width: 80, height: 80 }} />

                  <Typography sx={{ textAlign: "center" }}>
                    {user.username}
                  </Typography>
                </Box>

                <FormControl sx={{ m: 1, minWidth: 120 }}>
                  <Select
                    value={grade}
                    onChange={(event) => {
                      if (event.target.value !== user.grade) {
                        setIsDisabled(false);
                      } else setIsDisabled(true);

                      setGrade(event.target.value);
                    }}
                    displayEmpty
                    // defaultValue={userData.grade}
                    // label="Оцінка"
                  >
                    <MenuItem value="">Оцінка</MenuItem>
                    <MenuItem value={5}>5</MenuItem>
                    <MenuItem value={4}>4</MenuItem>
                    <MenuItem value={3}>3</MenuItem>
                    <MenuItem value={2}>2</MenuItem>
                    <MenuItem value={1}>1</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </Box>

            <TextField
              label="Коментар"
              multiline
              rows={5}
              value={comment}
              variant="filled"
              onChange={(e) => {
                if (e.target.value !== user.comment) {
                  setIsDisabled(false);
                } else setIsDisabled(true);

                setComment(e.target.value);
              }}
            />

            <Box sx={{ textAlign: "center" }}>
              <Button
                type="submit"
                variant="outlined"
                size="large"
                sx={{
                  textTransform: "capitalize",
                }}
                onClick={submit}
                disabled={isDisabled}
              >
                Надіслати
              </Button>
            </Box>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
