import { Button, Typography } from "@mui/material";
import MessageIcon from "@mui/icons-material/Message";

export const GradeButton = ({ handleOpen, grading: { grade, comment } }) => {
  const gradeValue = grade ? grade : "Оцінка";
  const commentValue = comment && (
    <MessageIcon color="primary" sx={{ width: 12, height: 12 }} />
  );

  let bg;
  let borderColor;

  if (grade <= 1) {
    bg = "rgba(221, 45, 45, 0.2)";
    borderColor = "#DD2D2D";
  } else if (grade <= 2) {
    bg = "rgba(240, 115, 45, 0.2)";
    borderColor = "#F0732D";
  } else if (grade <= 3) {
    bg = "rgba(240, 169, 45, 0.2)";
    borderColor = "#F0A92D";
  } else if (grade <= 4) {
    bg = "rgba(140, 173, 44, 0.2)";
    borderColor = "#8CAD2C";
  } else if (grade <= 5) {
    bg = "rgba(60, 167, 69, 0.2)";
    borderColor = "#3CA745";
  }
  if (grade === "") {
    bg = "primary";
    borderColor = "primary";
  }

  return (
    <Button
      type="submit"
      variant="outlined"
      size="large"
      sx={{
        display: "flex",
        gap: "2px",
        alignItems: "start",

        width: "75px",
        border: `1px solid ${borderColor}`,
        borderRadius: "4px",
        background: `${bg}`,
        color: grade !== "" && "black",
        textTransform: "capitalize",
      }}
      onClick={handleOpen}
    >
      {gradeValue}
      {commentValue}
    </Button>
  );
};
