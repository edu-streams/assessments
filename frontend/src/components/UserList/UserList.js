import { useEffect, useState } from "react";

import List from "@mui/material/List";
import Box from "@mui/material/Box";
import { User } from "./User";
import { Filter } from "./Filter";
import { fetchUsers } from "../../services/fetchUserData";
import { socket } from "../../services/socket";

export const UserList = ({ streamId }) => {
  const [filter, setFilter] = useState("");
  const [userArr, setUserArr] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await fetchUsers(streamId);

        if (data) {
          setUserArr(data);

          socket.on("userName", (userData) => {
            const findDuplicateUser = data.filter((user) => {
              return user.studentId === userData.studentId;
            });

            if (findDuplicateUser.length === 0) {
              console.log(7897987897879);
              setUserArr((prev) => [userData, ...prev]);
            }
          });
        }
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);

  const getFilteredUsers = (filter) => {
    const normaliseFilter = filter.toLowerCase();

    return userArr.filter((user) =>
      user.username.toLowerCase().includes(normaliseFilter)
    );
  };

  return (
    <Box
      sx={{
        padding: 2,
        margin: "0 auto",
        maxWidth: 400,
      }}
    >
      <Box>
        <Filter setFilter={setFilter} />

        <List
          sx={{
            width: "100%",
            overflow: "auto",
            maxHeight: "70vh",
            "& ul": { padding: 0 },
          }}
          subheader={<li />}
        >
          {getFilteredUsers(filter).map((user) => (
            <User key={user.studentId} user={user} streamId={streamId} />
          ))}
        </List>
      </Box>
    </Box>
  );
};
