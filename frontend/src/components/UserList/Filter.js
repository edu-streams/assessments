import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import SearchIcon from "@mui/icons-material/Search";

export const Filter = ({ setFilter }) => {
  return (
    <>
      <TextField
        hiddenLabel
        variant="filled"
        size="small"
        placeholder="Введіть для пошуку..."
        onChange={(e) => setFilter(e.target.value)}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
        sx={{
          width: "100%",
          marginBottom: 1,
          "& input": { padding: 1.5 },
        }}
      />
    </>
  );
};
