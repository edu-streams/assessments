import ListItem from "@mui/material/ListItem";

import Modalk from "./Modal";

export const User = ({ user, streamId }) => {
  return (
    <ListItem
      key={user.id || user.studentId}
      sx={{
        padding: "0 16px",
        display: "flex",
        justifyContent: "space-between",
        gap: 2,
      }}
    >
      <p>{user.username}</p>

      <Modalk user={user} streamId={streamId} />
    </ListItem>
  );
};
