import { Box, List, ListItem, Rating, Typography } from "@mui/material";
import styled from "@emotion/styled";

import StarIcon from "@mui/icons-material/Star";
import { SectionTitle } from "../../UI/SectionTitle";
import { GradeBox } from "../../UI/GradeBox";
import { calculateSubjectAverages } from "../../helpers/calculateSubjectAverages";

const StyledRating = styled(Rating)({
  "& .MuiRating-iconFilled": {
    color: "rgb(250,219,20)",
  },
});

export const GPAlist = ({ averageValue, gradings }) => {
  return (
    <Box mt={6}>
      <SectionTitle title="Середній бал" />

      <Box
        sx={{ display: "flex", alignItems: "center", gap: 4, flexWrap: "wrap" }}
      >
        <Box>
          <Box p={2} textAlign="center">
            <Typography fontSize={48}>
              {Math.round(averageValue.toFixed(1))}
            </Typography>

            <StyledRating
              name="average-score"
              value={averageValue}
              readOnly
              precision={0.1}
              size="large"
              emptyIcon={
                <StarIcon
                  style={{
                    opacity: 0.55,
                  }}
                  fontSize="inherit"
                />
              }
            />
          </Box>
        </Box>

        <List
          sx={{
            padding: "12px 24px",
            border: 1,
            borderRadius: "4px",
            width: "100%",
            maxWidth: 350,
            overflow: "auto",
            maxHeight: "50vh",

            "& li": {
              display: "flex",
              width: "100%",

              justifyContent: "space-between",
              padding: "5px 0",
            },
          }}
        >
          {calculateSubjectAverages(gradings).map(({ subjectId, average }) => (
            <ListItem key={subjectId}>
              {subjectId === "d415de43-f817-4f42-a563-341c787d9ae8" &&
                "Математика"}

              {subjectId === "440b5e46-6441-422a-bf58-4f8f6f11a2be" &&
                "Українська"}
              <GradeBox grade={Math.round(average.toFixed(1))} />
            </ListItem>
          ))}
        </List>
      </Box>
    </Box>
  );
};
