import { Box, List, ListItem, ListItemText, Typography } from "@mui/material";
import { SectionTitle } from "../../UI/SectionTitle";
import { GradeBox } from "../../UI/GradeBox";
import { useFormattedDate } from "../../hooks/useFormattedDate";

export const AssessmentsList = ({ gradings }) => {
  return (
    <Box sx={{ width: "100%" }}>
      <SectionTitle title="Оцінки" />

      <List
        sx={{
          padding: "12px 24px",
          border: 1,
          borderRadius: "4px",
          width: "100%",
          overflow: "auto",
          maxHeight: "65vh",

          "& li": {
            display: "flex",
            width: "100%",
            justifyContent: "space-between",
            padding: "5px 0",
            borderBottom: 1,
          },
        }}
      >
        {gradings[0] &&
          gradings.map((grade) => (
            <ListItem key={grade.id}>
              <ListItemText>
                <Typography fontWeight={500}>
                  {useFormattedDate(grade.date)}
                </Typography>

                <Typography color={"#6B7280"} fontSize={14}>
                  {grade.subjectId === "d415de43-f817-4f42-a563-341c787d9ae8" &&
                    "Математика"}
                  {grade.subjectId === "440b5e46-6441-422a-bf58-4f8f6f11a2be" &&
                    "Українська"}
                </Typography>

                <Typography fontWeight={500}>
                  {grade.comment ? grade.comment : "Немає коменту"}
                </Typography>
              </ListItemText>

              <GradeBox grade={grade.grade ? grade.grade : "Без оцінки"} />
            </ListItem>
          ))}
      </List>
    </Box>
  );
};
