import { Avatar, Box, Grid, Rating, Typography } from "@mui/material";
import styled from "@emotion/styled";
import { GradeBox } from "../../UI/GradeBox";
import { AssessmentsList } from "./AssessmentsList";
import { GPAlist } from "./GPAlist";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { getUserGradingsById } from "../../services/fetchUserData";
import { calculateOverallAverage } from "../../helpers/calculateOverallAverage";
import { Loading } from "../../UI/Loading";

const StyledRating = styled(Rating)({
  "& .MuiRating-iconFilled": {
    color: "rgb(250,219,20)",
  },
});

export const StudentCabinet = ({ studentId: { name, id, img } }) => {
  const [gradings, setGradings] = useState([]);
  const [averageValue, setAverageValue] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await getUserGradingsById(id);

        if (data) {
          setGradings(data);
          setAverageValue(calculateOverallAverage(data));
        }

        setIsLoading(false);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);

  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <Typography variant="h1" fontSize={30}>
            Student account
          </Typography>
          <Grid mt={2} container spacing={2}>
            <Grid item xs={6}>
              <Box>
                <Box sx={{ display: "flex", gap: 4, flexWrap: "wrap" }}>
                  <Avatar
                    sx={{ width: 100, height: 100 }}
                    alt={name}
                    src={img}
                  ></Avatar>

                  <Box>
                    <Typography
                      variant="h3"
                      fontSize={28}
                      maxWidth={350}
                      textAlign={"center"}
                    >
                      {name}
                    </Typography>

                    <Box
                      mt={3}
                      fontSize={24}
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        flexWrap: "wrap",
                        gap: 4,
                      }}
                    >
                      <Typography fontSize={24}> Середній бал: </Typography>
                      <GradeBox grade={Math.round(averageValue.toFixed(1))} />
                    </Box>
                  </Box>
                </Box>

                <GPAlist averageValue={averageValue} gradings={gradings} />
              </Box>
            </Grid>

            <Grid item xs={6}>
              <AssessmentsList gradings={gradings} />
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
};
